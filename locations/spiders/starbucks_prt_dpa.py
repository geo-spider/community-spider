import scrapy
import pycountry
from locations.categories import Code
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class StarbucksPRT(scrapy.Spider):
    name = "starbucks_prt_dpa"
    brand_name = 'Starbucks'
    spider_type = 'chain'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('prt').alpha_3]
    min_lat, max_lat = 36.961335, 42.154248
    min_lng, max_lng = -9.501785, -6.182470
    step = 0.1

    # stat_urls = ["https://www.starbucks.pt"]

    def start_requests(self):
        base_url = "https://www.starbucks.pt/api/v1/store-finder?latLng="
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                url = base_url + f"{lat * self.step}%2C{lng * self.step}"
                yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
            if row['description'] != 'Closed':
                data = {
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'ref': row['id'],
                    'addr_full': row['address'],
                    'phone': row['phoneNumber'],
                    'lat': float(row['coordinates']['lat']),
                    'lon': float(row['coordinates']['lng']),
                    'website': 'https://www.starbucks.pt',
                    "opening_hours": '',
                }
                days = {
                    'Monday': 'Mo',
                    'Tuesday': 'Tu',
                    'Wednesday': 'We',
                    'Thursday': 'Th',
                    'Friday': 'Fr',
                    'Saturday': 'Sa',
                    'Sunday': 'Su',
                }

                opening_hours = ''

                for hours in row['hoursNext7Days']:
                    name = hours['name']
                    if name == 'Today':
                        name = self.get_day_abbr()
                    elif name == 'Tomorrow':
                        name = self.get_day_abbr(0)
                    else:
                        name = days.get(name, '')
                    if (name and hours['description'] != 'Closed'):
                        start_time, end_time = hours['description'].split(
                            ' to ')
                        start_time = datetime.strptime(
                            start_time, '%I:%M %p').strftime('%H:%M')
                        end_time = datetime.strptime(
                            end_time, '%I:%M %p').strftime('%H:%M')
                        opening_hours += f'{name} {start_time}-{end_time}; '

                data['opening_hours'] = opening_hours.strip()
                yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=-1):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days]
