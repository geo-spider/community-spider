import scrapy
from locations.categories import Code
import pycountry
import re
import uuid
from locations.items import GeojsonPointItem
from bs4 import BeautifulSoup
import json

class NextcareSpider(scrapy.Spider):
    name = 'nextcare_us_dpa'
    brand_name = 'NextCare'
    spider_chain_id = "34649"
    spider_type = 'chain'
    spider_categories = [Code.MEDICAL_SERVICES_CLINICS.value]
    spider_countries = [pycountry.countries.lookup('us').alpha_3]
    allowed_domains = ['nextcare.com']

    # start_urls = ['https://nextcare.com/location-results/']

    headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36",
    }

    def start_requests(self):
        url = "https://nextcare.com/contact-us/"

        yield scrapy.Request(
            url=url,
            method="GET",
            headers=self.headers,
            callback=self.parse_contact,
        )
    
    def parse_contact(self, response):
        try:
            tag_with_phone = response.xpath("/html/body/main/div/div[1]/div/div/section[1]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div[1]/div/div/div/div/a/@href").get()
            phone = [re.search("\\d+", tag_with_phone).group()]
        except:
            phone = []
        
        yield scrapy.Request(
            url="https://nextcare.com/location-results/",
            method="GET",
            headers=self.headers,
            callback=self.parse,
            cb_kwargs=dict(phone=phone)
        )

    def parse(self, response, phone):
        soup = BeautifulSoup(response.text, features="lxml")
        script = soup.find("script", {"id": 'NEXTCARE-locations-js-js-extra'})
        script_content = script.string.replace("\\nvar nextcareObject = ", "").replace(";\\n", "").replace("\\\\", "")
    
        parsed_places = json.loads(script_content)

        for place in parsed_places.get("locations_data", []):
            feature = {
                'ref': uuid.uuid4().hex,
                'chain_id': "300071",
                'chain_name': "NextCare",
                'brand': "NextCare",
                'city': place.get("city", ""),
                'state': place.get("state", ""),
                'street': place.get("street", ""),
                'postcode': place.get("zipcode", ""),
                'country': place.get("country", ""),
                'website': 'https://nextcare.com',
                'phone': phone,
                'lat': float(place.get("lat", "")),
                'lon': float(place.get("lng", "")),
            }

            yield GeojsonPointItem(**feature)
