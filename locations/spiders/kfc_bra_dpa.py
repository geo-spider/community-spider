# -*- coding: utf-8 -*-

import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from lxml import etree
import json

class KfcBraSpider(scrapy.Spider):
    name = 'kfc_bra_dpa'
    brand_name = 'KFC'
    spider_type = 'chain'
    spider_chain_id = '1559'
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('br').alpha_3]
    allowed_domains = ['kfcbrasil.com.br']
    
    # start_urls = ["https://kfcbrasil.com.br/enderecos/"]
   
    def start_requests(self):
        '''
        Spider entrypoint. 
        Request chaining starts from here.
        '''
        url = "https://kfcbrasil.com.br/enderecos/"
        
        yield scrapy.Request(
            url=url,
            callback=self.parse_info_from_html,
            dont_filter = True,
        )


    def parse_info_from_html(self, response):
        tree = etree.HTML(response.text)
        script_path = tree.xpath('/html/body/div[2]/div//div[@class="col-md-12 padding-top lojas"]')[0]

        cities_tree_element = script_path.getchildren()

        cities = []
        markers: List[Dict[str, str]] = []

        Alagoas_was = False
        for city in cities_tree_element:
            cities.append(city.values()[0])

            city_xpath = f'//*[@id="{cities[-1]}"]/ul'
            city_list_of_li = tree.xpath(city_xpath)
            
            if Alagoas_was == False:
                city_list_of_li = [city_list_of_li[0]]
                Alagoas_was = True
            elif cities[-1] == 'alagoas':
                continue

            city_li_of_marker = city_list_of_li[0].getchildren()
            for marker in city_li_of_marker:

                marker_tags = marker.getchildren()
                marker_dict = {
                    'city': city.values()[0],
                    'name': marker_tags[0].text,
                    'full_addrs': (marker_tags[2].text + ", " if marker_tags[2].text is not None else "") + (marker_tags[3].text if marker_tags[3].text is not None else ""),
                    'opening_hours': marker_tags[4].text
                }

                markers.append(marker_dict)
        
        url = "https://kfcbrasil.com.br/enderecos/"
        
        yield scrapy.Request(
            url=url,
            callback=self.parse_info_from_script,
            cb_kwargs=dict(markers_html=markers),
            dont_filter = True,
        )


    def parse_info_from_script(self, response, markers_html):
        tree = etree.HTML(response.text)
        script_path = tree.xpath('/html/body/script[4]/text()')[0]

        start: int = script_path.find('const locations = ') + len('const locations = ')
        end: int = script_path.find(';', start)
        data = script_path[start+1:end-1]

        ind: int = data.find('},{')
        markers_script = []
        markers_script.append(data[:ind+1])
        while ind != -1:
            new_ind = data.find('},{', ind+1)
            if (new_ind != -1):
                markers_script.append(data[ind+2:new_ind+1])
            ind = new_ind
        markers_script.append(data[data.rfind('},{')+2:])
        markers_script: List[Dict[str, str]] = list(map(json.loads, markers_script))

        ind = len(markers_script) - 1
        deletion_made = False
        while ind >= 0 and deletion_made == False:
            if markers_script[ind]["name"] == "Maceió Shopping":
                markers_script.pop(ind)
                deletion_made = True
            ind -= 1

        url = "https://kfcbrasil.com.br/enderecos/"
        yield scrapy.Request(
            url=url,
            callback=self.parse,
            cb_kwargs=dict(markers_html=markers_html, markers_script=markers_script),
            dont_filter = True,
        )


    def parse_opening_hours(self, op_h_marker) -> str:
        op_h_marker = op_h_marker.replace(" às ", "-")
        op_h_marker = op_h_marker.replace(", ", "; ")
        op_h_marker = op_h_marker.replace(" | ", "; ")
        op_h_marker = op_h_marker.replace("Fechado", "off")

        days_portugues = {
            # For Upper-case
            "SEG": "Mo",
            "TER": "Tu",
            "QUA": "We",
            "QUI": "Th",
            "SEX": "Fr",
            "SÁB": "Sa",
            "DOM": "Su",

            # For Lower-case
            "seg": "Mo",
            "ter": "Tu",
            "qua": "We",
            "qui": "Th",
            "sex": "Fr",
            "sáb": "Sa",
            "dom": "Su",

            # For incorrect op_h_marker in web-page.
            "-s:": "-Sa:",
            "SAB": "Sa",
            "sab": "Sa",
            # "": "",
        }

        for day_abbr in days_portugues:
            op_h_marker = op_h_marker.replace(day_abbr, days_portugues[day_abbr])

        op_h_marker = op_h_marker.replace(": ", " ")
        
        return op_h_marker


    def parse(self, response, markers_html: List[Dict[str, str]], markers_script: List[Dict[str, str]]):
        '''
        Parse data according to GeojsonPointItem schema.
        Possible attributes: DATA_FORMAT.md.
        Scrapy check docs: https://docs.scrapy.org/en/latest/topics/contracts.html.

        @url https://kfcbrasil.com.br/enderecos/
        @returns items 120 150
        @cb_kwargs {markers_html: markers_html, markers_script: markers_script}
        @scrapes ref name city opening_hours addr_full website phone lat lon  
        '''


        phone = ['+55 11 91222-8833']

        id_marker = 0
        for row in markers_script:
            ind = 0
            while ind < len(markers_html) and markers_html[ind]['name'] != row['name']:
                ind += 1
            
            fast_food_marker = {
                'ref': id_marker,
                'chain_id': self.spider_chain_id,
                'chain_name': self.brand_name,
                'city': markers_html[ind]['city'],
                'opening_hours': self.parse_opening_hours(markers_html[ind]['opening_hours']),
                'addr_full': markers_html[ind]['full_addrs'],
                'website': 'https://kfcbrasil.com.br/',
                'phone': phone,
                'lat': float(row['lat']),
                'lon': float(row['lng']),                
            }

            id_marker += 1

            yield GeojsonPointItem(**fast_food_marker)