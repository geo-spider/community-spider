import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List
from bs4 import BeautifulSoup, Comment
import uuid
import re


class AmenBankTunisia(scrapy.Spider):
    name = 'amen_bank_tunisia_dpa'
    brand_name = "Amen Bank Tunisia"
    spider_type = 'chain'
    spider_chain_id = "23103"
    spider_categories = [Code.BANK.value]
    spider_countries = [pycountry.countries.lookup('tn').alpha_3]
    allowed_domains = ['www.amenbank.com.tn']

    # start_urls = ['https://www.amenbank.com.tn/fr/reseau-agences.html']

    def start_requests(self):
        url = "https://www.amenbank.com.tn/fr/reseau-agences.html"

        yield scrapy.FormRequest(
            url=url,
            method='GET',
            callback=self.parse_cities
        )

    def parse_cities(self, response):
        html = response.text
        soup = BeautifulSoup(html, "html.parser")
        conteiner = soup.find(
            "form", {"id": "SearchAgencyForm"}).find_all("option")
        for row in conteiner[1:]:
            form_data = {'gov': f'{row.text}'}
            yield scrapy.FormRequest(
                url="https://www.amenbank.com.tn/php/search.php",
                formdata=form_data,
                method='POST',
                callback=self.parse
            )

    def parse_coordinates(self, row):
        comment_with_link = row.find(
            string=lambda text: isinstance(text, Comment))
        pattern = re.compile(r"\d{1,2}\.\d+,\d{1,2}\.\d+")
        matches = re.findall(pattern, comment_with_link)
        lat = matches[0].split(",")[0]
        lon = matches[0].split(",")[1]

        return lat, lon

    def parse(self, response):
        html = response.text
        soup = BeautifulSoup(html, "html.parser")
        conteiner = soup.find("div", {"class": "row justify-content-start"})
        data = conteiner.find_all("div", {"class": "box_item bg-white mb-3"})

        for row in data:
            info = row.find_all("p", {"class": "mb-0"})
            lat, lon = self.parse_coordinates(row)
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': uuid.uuid4().hex,
                'addr_full': info[0].text,
                'city': info[1].text,
                'website': "https://www.amenbank.com.tn/",
                'email': info[3].text,
                'phone': (info[2].text).replace(" ", ""),
                'lat': lat,
                'lon': lon
            }

            yield GeojsonPointItem(**data)
