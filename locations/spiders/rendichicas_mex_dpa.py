import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem
import urllib.parse
import re


class RendichicasMex(scrapy.Spider):
    
    name = "rendichicas_mex_dpa"    
    brand_name = 'Rendichicas'
    spider_type = 'chain'
    spider_chain_id = "33758"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('mex').alpha_3]

    start_urls = ["https://rendichicas.com/wp-json/wpgmza/v1/marker-listing/base64eJyrVirIKHDOSSwuVrJSCg9w941yjInxTSzKTi3yySwuycxLj4lxTizKLy1OzVHSUcpNLIjPTAEqNVGqBQBtghRF"]

    def parse(self, response):

        json_data = response.json()['meta']
        for row in json_data:
            address = row['description']
            address = re.sub(r'<.*?>', ' ', address)
            address = address.split("Y")[0]

            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['id'],
                'name': row['title'],
                'addr_full': address,
                'lat': row['lat'],
                'lon': row['lng'],
                'website': 'https://rendichicas.com/estaciones/',
            }

            yield GeojsonPointItem(**data)