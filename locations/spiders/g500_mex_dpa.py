import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
import urllib

class G500_MEX(scrapy.Spider):
    name = "g500_mex"
    brand_name = 'G500'
    spider_type = "chain"
    spider_chain_id = "33756"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('mex').alpha_3]

    # start_urls = ["https://g500network.com/wp-admin/admin-ajax.php"]

    def start_requests(self):
        base_url = "https://g500network.com/wp-admin/admin-ajax.php"
        for _id in range(0, 700):
            form_data = {
                'action': 'get_station_byid',
                'id': str(_id)
            }
            encoded_form_data = urllib.parse.urlencode(form_data)
            
            yield scrapy.Request(url=base_url,
                body=encoded_form_data,
                method="POST",
                headers={'Content-Type': 'application/x-www-form-urlencoded'},
                callback=self.parse,
                meta={'id': _id}
            )


    def parse(self, response):
        ref = response.meta['id']
        json_data = response.json()
        for row in json_data:
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'brand': self.brand_name,
                'ref': ref,
                'name': row['esn'],
                'addr_full': row['est'] + ', ' + row['col'] +', ' + row['add'],
                'phone': row['tel'],
                'state': row['est'],
                'postcode': row['zip'],
                'lat': row['lat'],
                'lon': row['lon'],
                'website': 'https://g500network.com/estaciones/',
            }
            yield GeojsonPointItem(**data)