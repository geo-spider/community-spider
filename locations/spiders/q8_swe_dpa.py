import scrapy
import pycountry
import json
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
from datetime import datetime, timedelta
from locations.items import GeojsonPointItem


class Q8_SWE(scrapy.Spider):
    name = "q8_swe_dpa"
    brand_name = 'Q8'
    spider_type = 'chain'
    spider_chain_id = "82"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('swe').alpha_3]

    start_urls = ["https://www.okq8.se/-/Station/GetGlobalMapStations?appDataSource=9d780912-2801-4457-9376-16c48d02e688"]

    def format_schedule(self, opening_hours):
        if opening_hours is None:
            return 'off'

        weekdays = opening_hours.get('WeekDays', None)
        saturday = opening_hours.get('Saturday', None)
        sunday = opening_hours.get('Sunday', None)

        weekdays_string = weekdays['From'] + '-' + weekdays['To'] if weekdays and weekdays.get('From') and weekdays.get(
            'To') else 'off'
        saturday_string = saturday['From'] + '-' + saturday['To'] if saturday and saturday.get('From') and saturday.get(
            'To') else 'off'
        sunday_string = sunday['From'] + '-' + sunday['To'] if sunday and sunday.get('From') and sunday.get(
            'To') else 'off'

        schedule = f"Mo-Fr: {weekdays_string}; Sa: {saturday_string}; Su: {sunday_string}"
        return schedule

    def parse(self, response):
        json_data = response.json()['stations']
        for row in json_data:
            opening_hours=self.format_schedule(row['openingHours'])
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['stationNumber'],
                'addr_full': row['city'] + ' ,' + row['address'],
                'city': row['city'],
                'lat': row['position']['lat'],
                'lon': row['position']['lng'],
                'website': 'https://www.okq8.se/pa-stationen/bensinstationer/',
                'phone': row['phone'],
                'opening_hours': opening_hours,
            }
            yield GeojsonPointItem(**data)