import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict

class PizzaHutSpider_BEL(scrapy.Spider):
    name = 'pizza_hut_bel_dpa'
    brand_name = 'Pizza Hut'
    spider_type = 'chain'
    spider_chain_id = '1582'
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries= [pycountry.countries.lookup('bel').alpha_3]
    
    # start_urls = ["https://pizzahut.be"]

    def start_requests(self):
        base_url = "https://api.pizzahut.be/stores/"
        yield scrapy.Request(url=base_url, callback=self.twodrots)


    def twodrots(self, response):
        json_data=response.json().get("store")
        
        for i in range(300, 400):
            store_id = "/R" + str(i)
            url = response.url + store_id
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        json_data = response.json().get("store")

        properties = {
            "ref": json_data.get("code"),
            'chain_name': self.brand_name,
            'chain_id': self.spider_chain_id,
            "street": json_data.get("address").get("address1"),
            "city": json_data.get("address").get("city"),
            "postcode": json_data.get("address").get("zipCode"),
            "country": json_data.get("address").get("country"),
            "lat": json_data.get("address").get("coordinates").get("latitude"),
            "lon": json_data.get("address").get("coordinates").get("longitude"),
            "phone": json_data.get("contact").get("phone"),
            "email": json_data.get("contact").get("emailRecruitment"),
            "website": "https://restaurants.pizzahut.be/",
            "opening_hours": '',
        }

        days = {
            'maandag' : 'Mo',
            'dinsdag' : 'Tu',
            'woensdag' : 'We',
            'donderdag' : 'Th',
            'vrijdag' : 'Fr',
            'zaterdag' : 'Sa',
            'zondag' : 'Su',
        }

        opening = ''

        for hours in json_data.get("openingHours"):

            opening += hours.get("dayName")+" "+hours.get("shift1").split("/")[0] + "-" + hours.get("shift2").split("/")[0] + "; "

            for i, j in days.items():
                opening = opening.replace(i, j)

            properties["opening_hours"] = opening[:-2]

        yield GeojsonPointItem(**properties)
