# -*- coding: utf-8 -*-

import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class AegeanOilSpider(scrapy.Spider):
    name = "aegean_dpa"
    brand_name = "Aegean"
    spider_type = "chain"
    spider_chain_id = "1003"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('gr').alpha_3]
    allowed_domains = ["aegeanoil.com"]

    start_urls = ["https://aegeanoil.com/wp-content/themes/aegeanoil/station_data.json"]

    def parse(self, response):
        responseData = response.json()['json']

        for row in responseData:
            try:
                phone = row['ΤΗΛΕΦΩΝΟ']
            except:
                phone = ''

            op_hours = ''
            days = ['ΩΡΑΡΙΟ ΔΕΥΤΕΡΑ', 'ΩΡΑΡΙΟ ΤΡΙΤΗ', 'ΩΡΑΡΙΟ ΤΕΤΑΡΤΗ', 
            'ΩΡΑΡΙΟ ΠΕΜΠΤΗ', 'ΩΡΑΡΙΟ ΠΑΡΑΣΚΕΥΗ', 'ΩΡΑΡΙΟ ΣΑΒΒΑΤΟ', 'ΩΡΑΡΙΟ ΚΥΡΙΑΚΗ']
            if 'ΩΡΑΡΙΟ ΔΕΥΤΕΡΑ' in row.keys():
                osm_days = ['Mo', 'Tu', "We", "Th", 'Fr', 'Sa', 'Su']
                for i in range(7):
                    hours = row[days[i]]
                    hours = hours.replace(' ', '')
                    hours = hours.replace('|', ',')
                    if hours != '' and hours != 'ΚΛΕΙΣΤΟ':
                        op_hours += f'{osm_days[i]} {hours}; '
                    else:
                        continue          
            
            data = {
                'ref': row['ΚΩΔ ΠΕΛΑΤ'],
                'chain_name': "Aegean",
                'chain_id': "1003",
                'street': row['ΔΙΕΥΘΥΝΣΗ'],
                'city': row['ΠΟΛΗ'],
                'website': 'https://aegeanoil.com/',
                'phone': [phone],
                'opening_hours': op_hours,
                'lat': float(row['Latitude']),
                'lon': float(row['Longitude']),
            }

            yield GeojsonPointItem(**data)