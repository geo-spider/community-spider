# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import uuid

import re


class LAGAS_MEX(scrapy.Spider):
    
    name = 'la_gas_mex'
    brand_name = 'La Gas'
    spider_chain_id = "29691"
    spider_type = 'chain'
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('mex').alpha_3]
    allowed_domains = ['lagas.com.mx']

    start_urls = ["https://lagas.com.mx/ubicaciones"]

    def parse(self, response):
        tree = response
        body_path = f'//tr/td/text()'
        body = tree.xpath(body_path)
        
        for i in range(0,len(body)-4,4):
            title = body[i].get()

            address=body[i+3].get()

            city=body[i+1].get()

            state=body[i+2].get()

            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': uuid.uuid4().hex,
                'addr_full': city + ', ' + address,
                'city': city,
                'state': state,
                'website': 'https://lagas.com.mx/ubicaciones',
            }

            yield GeojsonPointItem(**data)
