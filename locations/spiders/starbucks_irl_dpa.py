import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from datetime import datetime, timedelta


class StarbucksIRL(scrapy.Spider):
    name = "starbucks_irl_dpa"
    brand_name = 'Starbucks'
    spider_type = 'chain'
    spider_chain_id = "1396"
    spider_categories = [Code.COFFEE_SHOP.value]
    spider_countries = [pycountry.countries.lookup('irl').alpha_3]
    min_lat, max_lat = 51.2220001, 55.6360001
    min_lng, max_lng = -10.6500001, -5.9960001
    step = 0.1

    # start_urls = ["https://www.starbucks.ie"]

    def start_requests(self):
        base_url = "https://www.starbucks.ie/api/v1/store-finder?latLng="
        for lat in range(int(self.min_lat / self.step), int(self.max_lat / self.step)):
            for lng in range(int(self.min_lng / self.step), int(self.max_lng / self.step)):
                if (self.is_inside_country_polygon(lat * self.step, lng * self.step)):
                    url = base_url + f"{lat * self.step}%2C{lng * self.step}"
                    yield scrapy.Request(url=url, callback=self.parse)

    def is_inside_country_polygon(self, lat, lng):
        country_hexagon_vertices = [
            (54.391825, -7.030374),
            (54.010150, -6.655419),
            (54.042483, -6.224810),
            (52.219067, -6.075842),
            (51.243383, -10.051254),
            (52.178133, -10.582103),
            (54.623415, -10.256587),
            (55.415152, -7.330901),
            (55.264605, -7.204629),
            (54.753430, -7.872035),
            (54.375527, -8.294945),
            (54.165694, -7.867416),
            (54.100814, -7.173051),
        ]

        n = len(country_hexagon_vertices)
        inside = False
        p1x, p1y = country_hexagon_vertices[0]
        for i in range(n + 1):
            p2x, p2y = country_hexagon_vertices[i % n]
            if lng > min(p1y, p2y):
                if lng <= max(p1y, p2y):
                    if lat <= max(p1x, p2x):
                        if p1y != p2y:
                            xinters = (lng - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                        if p1x == p2x or lat <= xinters:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside

    def parse(self, response):
        json_data = response.json()['stores']
        for row in json_data:
            if row['description'] != 'Closed':
                data = {
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'ref': row['id'],
                    'addr_full': row['address'],
                    'phone': row['phoneNumber'],
                    'lat': float(row['coordinates']['lat']),
                    'lon': float(row['coordinates']['lng']),
                    'website': 'https://www.starbucks.ie/store-locator?types=starbucks',
                    "opening_hours": '',
                }
                days = {
                    'Monday': 'Mo',
                    'Tuesday': 'Tu',
                    'Wednesday': 'We',
                    'Thursday': 'Th',
                    'Friday': 'Fr',
                    'Saturday': 'Sa',
                    'Sunday': 'Su',
                }

                opening_hours = ''

                for hours in row['hoursNext7Days']:
                    name = hours['name']
                    if name == 'Today':
                        name = self.get_day_abbr()
                    elif name == 'Tomorrow':
                        name = self.get_day_abbr(1)
                    else:
                        name = days.get(name, '')
                    if (name and hours['description'] != 'Closed'):
                        start_time, end_time = hours['description'].split(' to ')
                        start_time = datetime.strptime(start_time, '%I:%M %p').strftime('%H:%M')
                        end_time = datetime.strptime(end_time, '%I:%M %p').strftime('%H:%M')
                        opening_hours += f'{name} {start_time}-{end_time}; '

                data['opening_hours'] = opening_hours.strip()
                yield GeojsonPointItem(**data)

    def get_day_abbr(self, days=0):
        days += datetime.today().weekday()
        return ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'][days]
