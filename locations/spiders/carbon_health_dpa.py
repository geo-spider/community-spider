import scrapy
from locations.categories import Code
from locations.items import GeojsonPointItem
import pycountry
from typing import List
from bs4 import BeautifulSoup
import re
import json

class CarbonHealth(scrapy.Spider):
    name = 'carbon_health_dpa'
    brand_name = 'Carbon Health'
    spider_chain_id = "34648"
    spider_type = 'chain'
    spider_categories = [Code.HOSPITAL.value]
    spider_countries = [pycountry.countries.lookup('us').alpha_3]
    allowed_domains = ['carbonhealth.com']
    
    start_urls = ['https://carbonhealth.com/locations']
    
    def parse(self,response):
        soup = BeautifulSoup(response.text,'lxml')
        
        script_data = soup.find("script", {"id": "__NEXT_DATA__"}).string
        parsed_content = json.loads(script_data)
        locations = parsed_content.get("props", {}).get("initialState", {}).get("config", {}).get("locations", {})
        
        for item in locations:

            data = {
                "ref": item['id'],
                "street": item['address']['firstLine'],
                "city": item['address']['city'],
                "state": item['address']['state'],
                "postcode": item['address']['zip'],
                "phone": item['phoneNumber'],
                "website": 'https://carbonhealth.com',
                "store_url": f"https://carbonhealth.com/{item['slug']}",
                "lat": item['address']['latitude'],
                "lon": item['address']['longitude'],
            }
            
            try:
                data["opening_hours"]= "Su-Sa "+item['hours'][0]['from']+"-"+item['hours'][0]['to']
            except:
                pass
            yield GeojsonPointItem(data)
