# -*- coding: utf-8 -*-
from lxml import etree
import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict
import uuid

import re

class KFC_AUT(scrapy.Spider):
    name = 'kfc_aut_dpa'
    brand_name = 'KFC'
    spider_chain_id = "1559"
    spider_type = 'chain'
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('aut').alpha_3]
    allowed_domains = ['kfc.co.at']

    # start_urls = ["https://www.kfc.co.at"]

    def convert_schedule(self,schedule):
        result = ''
        for string in schedule:
            string = string.strip()
            if not string:
                continue

            result = result + string + ' '

        substrings = result.split('Montag', 1)
        first_part = substrings[0].strip()
        trimmed_string = first_part + 'Montag ' + substrings[1].split('Montag', 1)[0].strip()

        trimmed_string = trimmed_string.replace("Montag", "Mo")
        trimmed_string = trimmed_string.replace("Dienstag", "Tu")
        trimmed_string = trimmed_string.replace("Mittwoch", "We")
        trimmed_string = trimmed_string.replace("Donnerstag", "Th")
        trimmed_string = trimmed_string.replace("Freitag", "Fr")
        trimmed_string = trimmed_string.replace("Samstag", "Sa")
        trimmed_string = trimmed_string.replace("Sonntag", "Su")
        trimmed_string = trimmed_string.replace("bis","-")
        trimmed_string = trimmed_string.replace("Uhr", ";")
        trimmed_string = trimmed_string.replace("Geschlossen", "off")

        return trimmed_string

    def start_requests(self):
        
        url = "https://www.kfc.co.at/restaurants"

        yield scrapy.Request(
            url=url,
            callback=self.parse
        )

    def parse(self, response):

        tree = response

        for i in range(1, 12):
            # Получение названия
            title_path = f'/html/body/div[3]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[2]/div[{i}]/div[1]/strong/text()'
            title = tree.xpath(title_path).get()

            # Получение адреса
            street_path = f'/html/body/div[3]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[2]/div[{i}]/div[1]/div[1]/text()'
            street = tree.xpath(street_path).get().rstrip(',')

            data_path = f'/html/body/div[3]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[2]/div[{i}]/div[1]/div[2]/text()'
            data = tree.xpath(data_path).get()
            parts = data.split()
            postcode = parts[0]
            city = parts[1].rstrip(',')

            # Получение расписания
            schedule_path = f'/html/body/div[3]/div[1]/div/div/div/div[1]/div/div/div[2]/div/div[2]/div[{i}]/div[2]/text()'
            schedule = tree.xpath(schedule_path).getall()
            opening_hours=self.convert_schedule(schedule)

            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': uuid.uuid4().hex,
                'addr_full': city+', ' + street,
                'city': city,
                'postcode': postcode,
                'website': 'https://kfc.co.at',
                'opening_hours': opening_hours,
            }
            yield GeojsonPointItem(**data)
