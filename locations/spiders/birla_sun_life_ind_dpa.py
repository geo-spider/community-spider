import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code

class BirlaSunLifeSpider(scrapy.Spider):
    name = 'birla_sun_life_ind_dpa'
    brand_name = 'Birla Sun Life Insurance'
    spider_type = 'chain'
    spider_chain_id = '28229'
    spider_categories = [Code.FINANCE_AND_INSURANCE.value]
    spider_countries = [pycountry.countries.lookup('in').alpha_3]
    allowed_domains = ["lifeinsurance.adityabirlacapital.com"]
    
    # start_urls = ["https://lifeinsurance.adityabirlacapital.com"]

    states_in_india = [
        'Ahmedabad',
        'Andhra%20Pradesh',
        'Arunachal%20Pradesh',
        'Assam',
        'Bihar',
        'Chandigarh',
        'Chhattisgarh',
        'Chennai',
        'Delhi',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal%20Pradesh',
        'Jammu%20%26%20Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
        'Madhya%20Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Orissa',
        'Nagaland',
        'Odisha',
        'Puducherry',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil%20Nadu',
        'Telangana',
        'Tripura',
        'Uttar%20Pradesh',
        'Uttarakhand',
        'West%20Bengal'
    ]

    def start_requests(self):
        '''
        Spider entrypoint. 
        Request chaining starts from here.
        '''
        for state in self.states_in_india:
            url = f"https://live.mapmyindia.com/absli/getsearchData.htm?state={state}&city=&rad=8&type=Branch&area=&branchcat=1"
            
            yield scrapy.Request(
                url=url, 
                method='GET', 
                callback=self.parse
                )

    def parse(self, response):
        response_data = response.json()
        if response_data:
            for location in response_data:
                data = {
                    'ref': location.get("branch_code"),
                    'chain_name': self.brand_name,
                    'chain_id': self.spider_chain_id,
                    'addr_full': location.get("address"),
                    'postcode': location.get("branch_pincode"),
                    'state': location.get("branch_state"),
                    'website': 'https://lifeinsurance.adityabirlacapital.com',
                    'lat': location.get("lat"),
                    'lon': location.get("lon"),
                }
                yield GeojsonPointItem(**data)
        
        
