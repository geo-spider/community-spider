# -*- coding: utf-8 -*-
import scrapy
from locations.categories import Code
from locations.items import GeojsonPointItem
import pycountry


class IntermarcheSpider(scrapy.Spider):
    name = "intermarche_dpa"
    brand_name = "Intermarche"
    spider_type = "chain"
    spider_chain_id = "786"
    spider_categories = [Code.GROCERY.value]
    spider_countries = [pycountry.countries.lookup('pol').alpha_3]

    start_urls = ['https://intermarche.pl/wp-content/themes/intermarche/json/markers.json']

    def parse(self, response):
        data = response.json()

        for row in data:
            item = GeojsonPointItem()

            street_housenumber = row.get('street')
            city = row.get('city')
            country = 'Polska'
            postcode = row.get('zip')

            item['ref'] = row['uid']
            item['addr_full'] = f'{street_housenumber}, {city}, {country}, {postcode}'
            item['city'] = city
            item['postcode'] = postcode
            item['country'] = country
            item['phone'] = [row.get('phone')]
            item['website'] = 'https://intermarche.pl/'
            item['email'] = [row.get('email')]
            item['opening_hours'] = [f"Mo-Fr {row.get('open_mon_fri')}, Sa {row.get('open_sat')}, Su {row.get('open_sun')}"]
            item['lat'] = float(row.get('lat'))
            item['lon'] = float(row.get('lng'))

            yield item