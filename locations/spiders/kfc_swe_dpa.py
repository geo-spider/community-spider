import scrapy
import uuid
from scrapy.responsetypes import Response
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class KFC_SWE_Spider(scrapy.Spider):
    name = 'kfc_swe_dpa'
    brand_name = 'KFC'
    spider_type = 'chain'
    spider_chain_id = '1559'
    spider_categories = [Code.FAST_FOOD.value]
    spider_countries = [pycountry.countries.lookup('swe').alpha_3]
    
    start_urls = ['https://www.kfc.nu/hitta-oss/']

    def parse(self, response: Response):
        table = response.css('table.table.table-hover tbody tr')

        for row in table:
            url = row.css('td a::attr(href)').get()
            yield scrapy.Request(
                url=response.urljoin(url),
                callback=self.parse_data
                )


    def parse_data(self, response: Response):
        tree = response.css('div.row div:nth-child(2)')

        lat, lon = tree.css('dl.inline-dl.coordinates dd::text').getall()

        data = {
            'ref': uuid.uuid4().hex,
            'chain_name': self.brand_name,
            'chain_id': self.spider_chain_id,
            'addr_full': tree.css('dl.inline-dl.adress dd::text').get(),
            'phone': tree.css('dl.inline-dl.contact dd::text').get(),
            'lat': lat,
            'lon': lon,
            'opening_hours': ', '.join(list(self.parse_hours(response.css('div.row div:nth-child(3)')))),
            'store_url': response.url,
            'website': 'https://www.kfc.nu/'
        }

        return GeojsonPointItem(**data)


    def parse_hours(self, response):
        SWD = {
            'Måndag': 'Mo',
            'Tisdag': 'Tu',
            'Onsdag': 'We',
            'Torsdag': 'Th',
            'Fredag': 'Fr',
            'Lördag': 'Sa',
            'Söndag': 'Su'
        }

        timings = {}
        week = response.css('dl.inline-dl.open')
        key = [x.strip() for x in week.css('dd::text').getall()]
        value = week.css('dt::text').getall()

        for time, day in zip(key, value):
            if time in timings.keys():
                timings[time].append(day)
                continue
            timings[time] = [day]

        for time, day in timings.items():
            yield f'{SWD[day[0]]}-{SWD[day[-1]]} {time.split("-")[0]}:00-{time.split("-")[1]}:00'
