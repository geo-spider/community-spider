import scrapy
import pycountry
import uuid
import re
from locations.items import GeojsonPointItem
from locations.categories import Code


class UniprixSpider(scrapy.Spider):
    name = 'uniprix_dpa'
    brand_name = 'Uniprix'
    spider_type = 'chain'
    spider_chain_id = '4586'
    spider_categories = [Code.PHARMACY.value]
    spider_countries = [pycountry.countries.lookup('ca').alpha_3]
    allowed_domains = ['https://www.uniprix.com']

    start_urls = ['https://www.uniprix.com/api/sitecore/Pharmacy/Pharmacies?id=%7B1D46A582-AF24-4EC2-87BA-AFFC76B73967%7D']

    def parse_hours(self, hours_array):
        day_dict = {1: 'Su', 2: 'Mo', 3: 'Tu',
                    4: 'We', 5: 'Th', 6: 'Fr', 7: 'Sa'}
        opening_hours = []

        for d in hours_array:
            temp = re.sub('H', ':', d['openDuration'])
            hours = re.sub(' ', '', temp)
            temp2 = day_dict[d['dayIndex']] + ' ' + hours
            opening_hours.append(temp2)

        return ";".join(opening_hours)

    def parse(self, response):
        stores = response.json()

        for item in stores['pharmacies']:
            data = {
                'ref': item['pharmacyId'],
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'addr_full': item['address'],
                'phone': re.sub('[^0-9]+', '', item['phone']),
                'store_url': 'https://www.uniprix.com' + item['detailUrl'],
                'website': 'https://www.uniprix.com/',
                'opening_hours': self.parse_hours(item['storeOpeningHours']),
                'lat': float(item['location']['lat']),
                'lon': float(item['location']['lng']),
            }

            yield GeojsonPointItem(**data)
