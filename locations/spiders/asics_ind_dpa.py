import scrapy
import pycountry
from locations.categories import Code
from locations.items import GeojsonPointItem

class ASICS_IND(scrapy.Spider):
    
    name = "asics_ind_dpa"
    spider_type = 'chain'
    brand_name = 'ASICS'
    spider_chain_id = "32523"
    spider_categories = [Code.SPECIALTY_CLOTHING_STORE.value]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]

    # start_urls = ["https://www.asics.com"]

    def start_requests(self):
        cities = [
            'Mumbai', 'Delhi', 'Bangalore', 'Kolkata', 'Chennai', 'Hyderabad', 'Ahmedabad', 'Pune',
            'Jaipur', 'Kochi', 'Kota', 'Lucknow', 'Guwahati', 'Jaisalmer', 'Kanyakumari', 'Leh', 'Nagpur', 'Dhaka'
            ]
        
        for i in range(0, len(cities)+1):
            base_url = f"https://www.asics.com/in/en-in/store-locator.json?text={cities[i]}"
            
            yield scrapy.Request(
                url=base_url,
                callback=self.parse
                )

    def parse(self, response):
        json_data = response.json()['Stores']

        for row in json_data.values():
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['ID'],
                'city': row['City'],
                'addr_full': row['City'] + ', ' + row['Address'],
                'phone': row['Phone'],
                'state': row['Region'],
                'postcode': row['Postcode'],
                'lat': row['Latitude'],
                'lon': row['Longitude'],
                'store_url': 'https://www.asics.com' + row['URL'],
                'website': 'https://www.asics.com/in/en-in/store-locator',
            }

            yield GeojsonPointItem(**data)
