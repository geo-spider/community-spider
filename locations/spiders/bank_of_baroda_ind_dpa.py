import scrapy
import json
import pycountry
from locations.categories import Code
from locations.items import GeojsonPointItem


class BankOfBarodaSpider(scrapy.Spider):
    name = 'bank_of_baroda_ind_dpa'
    brand_name = 'Bank of Baroda'
    spider_type = "chain"
    spider_chain_id = "2494"
    spider_categories = [Code.BANK.value, Code.ATM.value]
    spider_countries = [pycountry.countries.lookup('ind').alpha_3]
    allowed_domains = ['www.bankofbaroda.in']

    start_urls = ['https://www.bankofbaroda.in/js/bob/countrywebsites/India/elobbyMaster.js?_=1688565383538']

    def parse(self, response):
        json_data = response.text.split("=")[1].strip()
        json_data = json_data.rstrip(";")

        data = json.loads(json_data)

        for entry in data:
            item = {
                'ref': entry['ATMID'],
                'chain_id': self.spider_chain_id,
                'chain_name': self.brand_name,
                'addr_full': entry["ADDRESS"],
                'website': 'https://www.bankofbaroda.in/',
                'city': entry["ZONE"],
                'state': entry["STATE NAME"],
                'lat': entry["LATITUDE"],
                'lon': entry["LONGITUDE"],
            }

            yield item
