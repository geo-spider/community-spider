import scrapy
import pycountry
import json
from locations.categories import Code
from locations.items import GeojsonPointItem

class TAMOIL_CHE(scrapy.Spider):
    name = "tamoil_che_dpa"
    brand_name = 'TAMOIL'
    spider_type = 'chain'
    spider_chain_id = "81"
    spider_categories = [Code.PETROL_GASOLINE_STATION.value]
    spider_countries = [pycountry.countries.lookup('che').alpha_3]

    start_urls = ["https://www.tamoil.ch/page-data/store-locator/page-data.json"]

    def parse(self, response):
        json_data = response.json()['result']['data']['allStation']['nodes']
        for row in json_data:
            data = {
                'chain_name': self.brand_name,
                'chain_id': self.spider_chain_id,
                'ref': row['id'],
                'addr_full': row['city']['name'] + ', ' + row['street'],
                'state': row['county'],
                'phone': row['phone_1'],
                'postcode': row['zip'],
                'lat': row['lat'],
                'lon': row['lng'],
                'website': 'https://www.tamoil.ch/store-locator',
                'store_url': 'https://www.tamoil.ch' + row['slug'],
            }
            yield GeojsonPointItem(**data)