import scrapy
from locations.items import GeojsonPointItem
from locations.categories import Code
import pycountry

class FortebankAtmsSpider(scrapy.Spider):
    name = "fortebank_atms_dpa"
    brand_name = "ForteBank"
    spider_type = "chain"
    spider_chain_id = "8277"
    spider_categories = [Code.ATM.value]
    spider_countries = [pycountry.countries.lookup('kz').alpha_3]
    allowed_domains = ["bank.forte.kz", "cms-strapi.forte.kz"]

    # start_urls = ['https://bank.forte.kz/']

    def start_requests(self):
        yield scrapy.Request(
            url="https://cms-strapi.forte.kz/atms?_limit=-1",
            method='get',
            callback=self.parse
        )

    def parse(self, response):
        '''
        @url https://cms-strapi.forte.kz/atms?_limit=-1
        @returns items 1064 1100
        @scrapes ref name addr_full city phone website opening_hours lat lon
        '''

        data = response.json()

        for row in data:
            item = GeojsonPointItem()

            worktime = "; ".join([
                "Mo " + row.get('mondayWorkTime'),
                "Tu " + row.get('tuesdayWorkTime'),
                "We " + row.get('wednesdayWorkTime'),
                "Th " + row.get('thursdayWorkTime'),
                "Fr  " + row.get('fridayWorkTime'),
                "Sa " + row.get('saturdayWorkTime'),
                "Su " + row.get('sundayWorkTime')
            ])

            worktime = worktime.replace("выходной", "off").replace("Выходной", "off").replace("Круглосуточно", "24/7")

            item['ref'] = row.get("id")
            item['addr_full'] = row.get('address')
            item['city'] = row.get('city')
            item['website'] = 'https://bank.forte.kz/'
            item['opening_hours'] = worktime
            item['lat'] = row.get('latitude')
            item['lon'] = row.get('longitude')
            
            yield item